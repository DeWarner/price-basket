FROM python:3.8.2

WORKDIR package

COPY . .

RUN pip3 install .

ENTRYPOINT ["/usr/local/bin/PriceBasket"]
