FROM python:3.8

RUN pip3 install tox pytest

WORKDIR package

COPY . .

CMD tox
