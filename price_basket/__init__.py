"""exporting Deal and Store classes and helper functions"""
import logging
from decimal import Decimal as deci
from price_basket.discounts import Discount, BadProductReference
from price_basket.baskets import Store


def normalise_prices(prices):
    """return a prices dictionary with lowercase product names and decimal prices"""
    return {product.lower(): deci(str(price)) for product, price in prices.items()}


def generate_deals(prices, deals):
    """return a list of Discount objects ready for the store to be initialised with"""
    try:
        return [Discount.new(*deal[1].split(), name=deal[0], prices=prices) for deal in deals]
    except BadProductReference as error:
        log = logging.getLogger("")
        if error.product.lower() in prices:
            log.error(
                "Deal definition contains reference to non lowercase product name %s, "
                "please change to lowercase.", error.product
            )
            raise SystemExit(3)
        log.error("Could not set up deal on unknown product, please define a price for '%s'.", error.product)
        raise SystemExit(4)


def setup_store(prices, deals):
    """setup a store object with lowercase product names and the promotions initialised"""
    prices = normalise_prices(prices)
    offers = generate_deals(prices, deals)
    return Store(prices, offers)
