"""Store and PriceBasket class definitions held here, the main logic for the program
store holds pricing information, deals and how to format price which might"""
from collections import Counter
import logging
from typing import List


class Store:
    """a store is a configuration of prices and offers to generate baskets from,
    with a function that formats prices into a string with the relevant currency"""
    def __init__(self, prices: dict, offers: list, repr_price=None):
        self._prices = prices
        self._offers = offers
        self._repr_price = repr_price

    @property
    def prices(self):
        """prices should not be modified"""
        return self._prices.copy()

    @property
    def offers(self):
        """offers should not be modified after instantiation"""
        return self._offers

    def repr_price(self, price):
        """format a price into a string representation showing the currency, etc."""
        if self._repr_price is not None:
            return self._repr_price()
        sign = "-" * (price < 0)
        price = abs(price)
        if price >= 1:
            return "{0}£{1:.2f}".format(sign, price)
        return "{}{}p".format(sign, round(price * 100))

    def new_basket(self, items: List[str]):
        """generate a new basket object to calculate total for"""
        return PriceBasket(self, items)


class PriceBasket:
    """a basket holds a store object to determine offers and prices for items in the basket,
    basket behaves like a Counter object
    but also tracks prices and offers to calulate a total price"""
    def __init__(self, store: Store, items: List[str]):
        self.log = logging.getLogger("")
        self._store = store
        self._items = Counter()
        self._subtotal = 0
        self.deals = []
        self.add_items(items)

    def __getitem__(self, item):
        return self._items[item]

    def items(self):
        """forward the items function on to the counter object itself,
        so it can be treated in a similar way to as if the basket was a counter itself"""
        return self._items.items()

    def add_items(self, products: List[str]):
        """to avoid recalculation of deals after adding each item,
        this method accepts a list of items,
        adds all of the items in the list and then calculates the deals afterwards"""
        for product in products:
            self._add_item(product)
        self.deals = self.calculate_deals()

    def _add_item(self, product: str, count: int = 1):
        """add 1 item type (product) in quantity (count), and add the price to subtotal"""
        try:
            price = self._store.prices[product]
        except KeyError:
            self.log.error("No price found for %s, exiting program.", product)
            raise SystemExit(5)
        else:
            self._items[product] += count
            self._subtotal += price * count

    def calculate_deals(self):
        """loop through deals availiable in the store and apply each offer to the basket
        filter out offers that do not affect the total (discount==0)"""
        deals = [(offer.name, offer(self)) for offer in self._store.offers]
        return list(filter(lambda deal: (deal[1] != 0), deals))

    def calculate_subtotal(self):
        """calculate the subtotal for all of the items that are already in the basket"""
        subtotal = 0
        for product, count in self.items():
            subtotal += self._store.prices[product] * count
        return subtotal

    @property
    def subtotal(self):
        """accessor to subtotal, since subtotal should not be modified directly"""
        return self._subtotal

    @property
    def total(self):
        """calculate the total by subtracting the discounts from the subtotal"""
        return self._subtotal - sum([deal[1] for deal in self.deals])

    @property
    def summary(self):
        """return a representation of the basket informing the user of
        subtotal, discounts and total"""
        summary_lines = []
        summary_lines.append("Subtotal: {}".format(self._store.repr_price(self._subtotal)))
        if not list(self.deals):
            summary_lines.append("(no offers available)")
        for deal in self.deals:
            summary_lines.append("{}: {}".format(deal[0], self._store.repr_price(-deal[1])))
        summary_lines.append("Total: {}".format(self._store.repr_price(self.total)))
        return "\n".join(summary_lines)
