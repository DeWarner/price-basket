"""definitions for discount types"""
from decimal import Decimal as deci


class BadProductReference(Exception):
    """Exception that is rased when a deal applies to a product that hasn't been defined"""
    def __init__(self, product):
        super().__init__()
        self.product = product


class Discount:
    """base class for Discounts"""
    def __init__(self, name=None, prices=None):
        self.prices = prices
        self.name = name

    def verify_product(self, product):
        """verify product method reduces boiler plate by raising the correct exception if the deal
        references an undefined product"""
        if product not in self.prices:
            raise BadProductReference(product)
        return product

    def __call__(self, basket):
        """abstract class placeholder, should never be called"""
        raise NotImplementedError("Abstract class")

    @staticmethod
    def new(discount_type, *args, **kwargs):
        """load the right deal subclass from the string representation
        raise exception if appropriate subclass is not found"""
        for subclass in Discount.__subclasses__():
            if discount_type == subclass.__name__:
                return subclass(*args, **kwargs)
        raise NotImplementedError("Abstract class")


class StraightDiscount(Discount):
    """the most straightforward deal type eg. (X% off product Y),
    discount is given as a multiplier instead of percentage,
    i.e. if you want 10 percent off discount = 0.1"""
    def __init__(self, item_type, discount, **kwargs):
        super().__init__(**kwargs)
        self.item_type = self.verify_product(item_type)
        self.discount = deci(discount)

    def __call__(self, basket):
        """return the total discount to apply
        multiply the number of items of the designated product type
        by the provided discount multipier"""
        discount_per_item = self.prices[self.item_type] * self.discount
        return basket[self.item_type] * discount_per_item


class MultiBuyDiscount(Discount):
    """more involved discount type, e.g. for every 2 soups you buy, get discount on 1 soup"""
    def __init__(self, buy_type, buy_number, get_type, discount, **kwargs):
        super().__init__(**kwargs)
        self.buy_type = self.verify_product(buy_type)
        self.buy_number = deci(buy_number)
        self.get_type = self.verify_product(get_type)
        self.discount = deci(discount)

    def __call__(self, basket):
        """return the total discount to apply
        calculate limit on get_type discounts by floor division on the count of buy_type
        and then apply the discount as per the limit"""
        discount_per_item = self.prices[self.get_type] * self.discount
        limit = basket[self.buy_type] // self.buy_number
        return min(limit, basket[self.get_type]) * discount_per_item
