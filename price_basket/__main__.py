#! python3.8
"""the main program flow, default setup as described in the assignment,
store config could potentially be loaded in from db, api or config file"""
import sys
from price_basket import setup_store


def main(args):
    """high level program code"""
    prices = dict(soup=0.65, bread=0.8, milk=1.3, apples=1.0)
    deals = [
        ("Apples 10% off", "StraightDiscount apples 0.1"),
        ("Buy 2 Soups, get Bread half price", "MultiBuyDiscount soup 2 bread 0.5"),
    ]
    basket_contents = (product.lower() for product in args)
    store = setup_store(prices, deals)
    basket = store.new_basket(basket_contents)
    print(basket.summary)


USAGE = "\n".join([
    "Invalid Arguments (options detected)",
    f"usage: {sys.argv[0]} [product]...",
    "Calculate cost of products provided according to the store.",
    "In the store configuration products must be defined in lower case.",
    "Deals should be defined referring to products in lower case also.",
    "Products can be supplied case insensitive in command line args.",
    "Deals in configuration must follow one of the following schemas:",
    "\tStraightDiscount [product] [multiplier]",
    "\tMultiBuyDiscount [product_to_buy] [count] [product_to_discount] [multiplier]",
])

if __name__ == "__main__":
    if [item for item in sys.argv[1:] if item[0] == "-"]:
        print(USAGE, file=sys.stderr)
        raise SystemExit(1)
    main(sys.argv[1:])
