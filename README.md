# PriceBasket

Built for Python3.8.2
A cli tool for pricing shopping baskets, taking promotions into account

## Quick Start

### Docker

``` sh
docker run registry.gitlab.com/dewarner/price-basket [Items]...
```

### Python 3.8

``` sh
git clone https://gitlab.com/DeWarner/price-basket.git
python3 -m pip install ./price-basket
python3 -m price_basket [Items]...
```

The pip installation proceedure should also place the PriceBasket executable in your path, so it can be called by...

``` sh
PriceBasket [Items]...
```

