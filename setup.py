#!/usr/bin/env python

from setuptools import setup, find_packages

setup(
    name='PriceBasket',
    version='1.0',
    description='price items and apply promotions',
    author='Declan Warner',
    author_email='dwarn@protonmail.com',
    url='https://gitlab.com/DeWarner/price-basket.git',
    packages=find_packages(),
    scripts=["PriceBasket"],
)
