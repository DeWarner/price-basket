import pytest
from decimal import Decimal as deci
from collections import Counter as count
from price_basket import discounts as d


def test_DiscountFactory():
    with pytest.raises(NotImplementedError):
        d.Discount.new("NotARealDiscount")


def test_StraightDiscount1():
    cheaper_apples = d.Discount.new("StraightDiscount", "apples", "0.3", prices={"apples": 1})
    assert cheaper_apples(count(apples=10)) == 3


def test_StraightDiscount2():
    with pytest.raises(d.BadProductReference):
        d.StraightDiscount("Apples", "0.3", prices={"apples": 1})


def test_StraightDiscount3():
    cheaper_apples = d.StraightDiscount("apples", "0.3", prices={"apples": 1})
    assert cheaper_apples(count(Apples=2)) == 0


def test_MultiBuyDiscount1():
    multibuy = d.Discount.new(
        "MultiBuyDiscount", "soup", "3", "bread", "0.01",
        prices={"soup": 1, "bread": 2},
    )
    assert multibuy(count(soup=10, bread=5)) == deci("0.06")


def test_MultiBuyDiscount20():
    with pytest.raises(d.BadProductReference):
        d.MultiBuyDiscount("soup", "3", "yams", "0.2", prices={"yams": 1})


def test_MultiBuyDiscount21():
    with pytest.raises(d.BadProductReference):
        d.MultiBuyDiscount("soup", "3", "yams", "0.2", prices={"soup": 1})


def test_MultiBuyDiscount3():
    multibuy = d.MultiBuyDiscount(
        "soup", "3", "bread", "0.2",
        prices={"soup": 1, "bread": 3},
    )
    assert multibuy(count()) == 0
