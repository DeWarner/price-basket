"""test functions defined in __init__.py"""
from decimal import Decimal
import pytest
from price_basket import normalise_prices, generate_deals


def test_normalise_prices_1():
    prices = normalise_prices({"apples": 12.3, "Potatoes": 23, "LeMoNs": "1.32"})
    assert all([isinstance(key, Decimal) for key in prices.values()])
    assert prices == {"apples": Decimal("12.3"), "potatoes": 23, "lemons": Decimal("1.32")}


def test_generate_deals():
    prices = normalise_prices({"apples": 12.3, "Potatoes": 23, "LeMoNs": "1.32"})
    with pytest.raises(SystemExit):
        generate_deals(prices, [
            ("the deal description", "StraightDiscount avocados 0.5")
        ])
