from decimal import Decimal as deci
from price_basket import setup_store

def test_full_example_1():
    store = setup_store(
        dict(soup="0.65", bread="0.8", milk="1.3", apples="1"),
        [
            ("Cheaper Apples", "StraightDiscount apples 0.1"),
            ("you want bread with your soup?", "MultiBuyDiscount soup 2 bread 0.5"),
        ],
    )
    basket = store.new_basket("apples milk bread".split())
    assert basket.subtotal == deci("3.1")
    assert basket.deals == [
        ("Cheaper Apples", deci("0.1")),
    ]
    assert basket.total == 3


def test_full_example_2():
    store = setup_store(
        dict(soup="0.65", bread="0.8", milk="1.3", apples="1"),
        [
            ("Cheaper Apples", "StraightDiscount apples 0.1"),
            ("you want bread with your soup?", "MultiBuyDiscount soup 2 bread 0.5"),
        ],
    )
    basket = store.new_basket("apples milk bread".split())
    assert basket.subtotal == deci("3.1")
    assert basket.deals == [
        ("Cheaper Apples", deci("0.1")),
    ]
    assert basket.total == deci(3)
